 
# Script for setting up CentOS8 machine as 'docker buildx'-capable GitLab runner
# (Tested on CERN OpenStack VMs)

# Tom Williams, June 2021


# ARG PARSING
if [ $# -eq "1" ]
then
  RUNNER_REGISTRATION_TOKEN=$1
else
  echo "ERROR: Invalid number of arguments!"
  echo "usage: $0 RUNNER_REGISTRATION_TOKEN"
  exit 1
fi

set -e
set -x




# STEP 0: Command run as root to get access to OpenStack VM
#   dnf install locmap-release
#   dnf install locmap
#   /usr/bin/locmap --enable kerberos
#   /usr/bin/locmap --enable afs
#   /usr/bin/locmap --configure all
#   # Then add username to /etc/sudoers



# STEP 1: Install docker

if [[ ! $(groups) =~ "docker" ]]; then
    sudo dnf install -y https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.13-3.2.el7.x86_64.rpm
    sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    sudo sed -i 's|\$releasever|7|g' /etc/yum.repos.d/docker-ce.repo 
    sudo dnf install -y docker-ce-19.03.11 docker-ce-cli-19.03.11
    sudo systemctl enable --now docker
    systemctl is-active docker
    sudo usermod -aG docker $USER
    set +x
    echo -e "\n\nPlease re-run this script after logging out and logging back in, to pick up new group membership"
    exit 0
fi

# Docker/CentOS8 firewall workaround: Add masquerade, then restart docker. Sources:
#  - https://github.com/docker/for-linux/issues/957#issuecomment-657632706
#  - https://benjaminberhault.com//post/2020/08/15/enable-network-connectivity-between-docker-containers-on-centos-8.html

sudo firewall-cmd --zone=public --add-masquerade --permanent
sudo firewall-cmd --reload
sudo systemctl restart docker
sudo systemctl is-active docker

# Verify that DNS working in containers
docker run centos:8 getent hosts google.com




# STEP 2: Install QEMU and binfmt-support package
#         Docker-based solution; QEMU simulators stay registered until next reboot.

# docker run --rm --privileged multiarch/qemu-user-static:5.2.0-2 --reset -p yes
sudo tee /etc/systemd/system/docker-multiarch-qemu.service << EOF
[Unit]
Description=Runs docker container that registers QEMU multiarch simulators in host 
After=docker.service
BindsTo=docker.service
ReloadPropagatedFrom=docker.service

[Service]
Type=oneshot
ExecStart=docker run --rm --privileged multiarch/qemu-user-static:5.2.0-2 --reset -p yes
ExecReload=docker run --rm --privileged multiarch/qemu-user-static:5.2.0-2 --reset -p yes
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
EOF




# STEP 3: Enable experimental CLI (for 'docker buildx')

mkdir -p ~/.docker
if [[ ! -s ~/.docker/config.json ]]
then
    echo '{}' > ~/.docker/config.json
fi
sudo dnf install -y jq
jq '. + { "experimental" : "enabled" }' ~/.docker/config.json > ~/.docker/config.json.mod
mv ~/.docker/config.json.mod ~/.docker/config.json

# Check that experimental CLI is now enabled
docker buildx --help




# STEP 4: Create buildx builder

docker buildx rm mybuilder
docker buildx create --name mybuilder
docker buildx use mybuilder
docker buildx inspect --bootstrap
docker buildx ls

# Check that buildx is now working
echo -e 'FROM alpine:latest\nRUN echo “Running on $(uname -m)”' > buildx-test.Dockerfile
docker buildx build --no-cache --progress plain --platform linux/arm/v7,linux/arm64,linux/amd64 -t buildx-test-image -f buildx-test.Dockerfile .




# STEP 5: Enable experimental mode of docker daemon (for 'docker run --platform')

mkdir -p ~/.docker
if [[ ! -s /etc/docker/daemon.json ]]
then
    echo '{}' | sudo tee /etc/docker/daemon.json
fi
jq '. + { "experimental" : true }' /etc/docker/daemon.json | sudo tee /etc/docker/daemon.json.mod
sudo mv /etc/docker/daemon.json.mod /etc/docker/daemon.json
sudo systemctl restart docker




# STEP 6: Install GitLab runner & register with GitLab
curl -LJ "https://gitlab-runner-downloads.s3.amazonaws.com/v13.12.0/rpm/gitlab-runner_amd64.rpm" -o /tmp/gitlab-runner_amd64.rpm
sudo dnf localinstall -y /tmp/gitlab-runner_amd64.rpm
sudo systemctl is-active gitlab-runner

sudo gitlab-runner register --non-interactive \
  --url https://gitlab.cern.ch \
  --registration-token=${RUNNER_REGISTRATION_TOKEN} \
  --description="DinD runner for cross-building images [$(hostname)]" \
  --executor=docker --tag-list=docker:dind,multiarch:qemu \
  --run-untagged=false \
  --docker-image=docker:19.03.11 \
  --docker-privileged=true \
  --env DOCKER_DRIVER=overlay2 \
  --docker-volumes /cache \
  --docker-volumes /certs \
  --docker-allowed-images jonoh/docker-buildx-qemu:19.03.8_0.3.1 \
  --docker-allowed-services docker:19.03.11-dind
