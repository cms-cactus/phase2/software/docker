
function exitCodeText() {
    rc=$?
    if [ $rc != 0 ]; then
        echo -e "exit code $rc; "
    fi
    return $rc
}

function codeColor() {
   rc=$?
   if [ $rc = 0 ]; then
       color="1;32m"
   else
       color='1;31m'
   fi
   echo -e "${color}"
   return $rc;
}


# Functions for measuring time taken by last command - heavily based on https://jakemccrary.com/blog/2015/05/03/put-the-last-commands-run-time-in-your-bash-prompt/
function timer_start {
  timer=${timer:-$SECONDS}
}

function timer_stop {
  timer_value_seconds="$(($SECONDS - $timer))"
  if [ "$timer_value_seconds" -gt "60" ];
  then
    timer_value_display="$(($timer_value_seconds / 60))min $(($timer_value_seconds % 60))s"
  else
    timer_value_display="${timer_value_seconds}s"
  fi
  unset timer
}

trap 'timer_start' DEBUG

if [ "$PROMPT_COMMAND" == "" ]; then
    PROMPT_COMMAND="timer_stop"
else
    PROMPT_COMMAND="$PROMPT_COMMAND; timer_stop"
fi


parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}


export PS1='\[\e[$(codeColor)\][$(exitCodeText)took ${timer_value_display}] [\t] \[\033[0;34m\]\u@\h \[\033[33m\]$(parse_git_branch)\[\033[0m\]\n \[\033[34m\]\w\[\033[00m\] > '
