
FROM almalinux:8.9-20231124 as common

LABEL author "Tom Williams <tom.williams@cern.ch>"


# Install common RPMs (required at runtime and for build)
RUN \
  dnf -y install epel-release && \
  dnf -y install glibc libgcc \
    boost-filesystem boost-program-options boost-system boost-test boost-thread \
    jsoncpp log4cplus yaml-cpp && \
  dnf clean all




# Base image for development (contains build tools and headers)
FROM common as development

COPY bash_prompt.sh /etc/profile.d/prompt.sh

RUN \
  dnf -y install \
    cmake make rpm-build createrepo git tree man-pages which \
    gcc-c++-8.* clang-16.* clang-tools-extra-16.* \
    boost-devel jsoncpp-devel log4cplus-devel yaml-cpp-devel zlib-devel \
    # For doxygen build
    python38 bison flex && \
  dnf clean all && \
  # Install doxygen (build from scratch to get recent version - only v1.8.14 from 2017 available from EPEL)
  cd /tmp && \
  curl -O https://www.doxygen.nl/files/doxygen-1.9.8.src.tar.gz && \
  tar xzf doxygen-1.9.8.src.tar.gz && \
  cd doxygen-1.9.8 && \
  mkdir build && cd build && \
  cmake -G "Unix Makefiles" .. && make -j$(nproc) && make install && \
  # Install include-what-you-use (build from scratch as no longer included with clang)
  cd /tmp && \
  curl -L https://github.com/include-what-you-use/include-what-you-use/archive/refs/tags/0.20.tar.gz -o include-what-you-use.tar.gz && \
  tar xzf include-what-you-use.tar.gz && \
  cd include-what-you-use-0.20 && \
  dnf -y install clang-devel-16.* libffi-devel llvm-devel && \
  mkdir build && cd build && \
  cmake -G "Unix Makefiles" .. && make && make install && \
  # Clean up from doxygen & include-what-you-use install
  cd / && rm -rf /tmp/doxygen* /tmp/include-what-you-use* && \
  dnf -y remove clang-devel libffi-devel llvm llvm-test llvm-static llvm-devel ncurses-c++-libs ncurses-devel libedit-devel && \
  dnf clean all



# Default user & command
USER root
ENTRYPOINT ["/bin/bash", "-c"]
CMD ["bash"]




# Base image for runtime (only libraries - all already installed above)
FROM common as production

# Default user & command
USER root
ENTRYPOINT ["/bin/bash", "-c"]
CMD ["bash"]
