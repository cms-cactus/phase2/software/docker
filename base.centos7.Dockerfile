FROM centos:7.9.2009 as common

LABEL author "Tom Williams <tom.williams@cern.ch>"

ARG TARGETPLATFORM


# Install common RPMs (required at runtime and for build)
RUN \
  if [ "${TARGETPLATFORM}" == "linux/arm/v7" ]; then \
    echo "armhfp" > /etc/yum/vars/basearch; \
    echo "armv7hl" > /etc/yum/vars/arch; \
    echo "armv7hl-redhat-linux-gpu" > /etc/rpm/platform; \
    echo -e "[epel]\nname=Epel rebuild for armhfp\nbaseurl=https://armv7.dev.centos.org/repodir/epel-pass-1/\nenabled=1\ngpgcheck=0" > /etc/yum.repos.d/epel.repo; \
    mkdir -p /usr/local/lib64; \
  else \
    yum -y install epel-release centos-release-scl; \
  fi && \
  yum -y install glibc libgcc \
    boost-filesystem boost-program-options boost-system boost-test boost-thread \
    jsoncpp log4cplus yaml-cpp && \
  yum clean all




# Base image for development (contains build tools and headers)
FROM common as development

COPY bash_prompt.sh /etc/profile.d/prompt.sh

RUN \
  # Must comment out 'tsflags=nodocs' for 'man' command to work
  sed -i "s/tsflags=nodocs/# tsflags=nodocs/g" /etc/yum.conf && \
  yum -y install make rpm-build createrepo rh-git227-git tree man-pages man-db which \
    devtoolset-8-gcc-c++ \
    llvm-toolset-7.0-clang llvm-toolset-7.0-clang-tools-extra \
    boost-devel jsoncpp-devel log4cplus-devel yaml-cpp-devel zlib-devel && \
  # Install cmake3 from source if 3.17 not available (i.e. for arm64)
  yum -y install dos2unix openssl-devel unzip && \
  cd /tmp && \
  curl -LO https://github.com/Kitware/CMake/releases/download/v3.20.2/cmake-3.20.2.zip && \
  unzip cmake-3.20.2.zip && \
  cd cmake-3.20.2 && \
  find . -type f -print0 | xargs -0 dos2unix || true && \
  scl enable devtoolset-8 'time ./bootstrap --prefix=/usr && time make -j$(nproc) && time make install' && \
  ln -s /usr/bin/cmake /usr/bin/cmake3 && \
  yum clean all && \
  # Always source devtoolset-8 even if entrypoint overridden (e.g. as happens in VSCode devcontainers)
  echo "source scl_source enable devtoolset-8" > /etc/profile.d/devtoolset8.sh && \
  echo "source scl_source enable llvm-toolset-7.0" > /etc/profile.d/llvm-toolset-7.0.sh

# Add git 2.27 directories to ${PATH} etc., so that VSCode detects that git is present when
# while starting up devcontainer - otherwise user .gitconfig from host OS won't be mounted.
# Exact variable modifications taken from /opt/rh/rh-git227/enable
ENV \
  PATH=/opt/rh/rh-git227/root/usr/bin${PATH:+:${PATH}} \
  MANPATH=/opt/rh/rh-git227/root/usr/share/man:${MANPATH} \
  PERL5LIB=/opt/rh/rh-git227/root/usr/share/perl5/vendor_perl${PERL5LIB:+:${PERL5LIB}} \
  LD_LIBRARY_PATH=/opt/rh/httpd24/root/usr/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}

# Default user & command (use login shell for image builds, so that /etc/profile is sourced)
USER root
SHELL      ["/bin/bash", "--login", "-c"]
ENTRYPOINT ["/bin/bash", "-c"]
CMD ["bash"]




# Base image for runtime (only libraries - all already installed above)
FROM common as production

# Default user & command
USER root
ENTRYPOINT ["/bin/bash", "-c"]
CMD ["bash"]
