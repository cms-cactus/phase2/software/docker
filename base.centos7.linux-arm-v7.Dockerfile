FROM centos:7.9.2009 as common

LABEL author "Tom Williams <tom.williams@cern.ch>"

ARG TARGETPLATFORM


# Install common RPMs (required at runtime and for build)
RUN \
  if [ "${TARGETPLATFORM}" == "linux/arm/v7" ]; then \
    echo "armhfp" > /etc/yum/vars/basearch; \
    echo "armv7hl" > /etc/yum/vars/arch; \
    echo "armv7hl-redhat-linux-gpu" > /etc/rpm/platform; \
    echo -e "[epel]\nname=Epel rebuild for armhfp\nbaseurl=https://armv7.dev.centos.org/repodir/epel-pass-1/\nenabled=1\ngpgcheck=0" > /etc/yum.repos.d/epel.repo; \
    mkdir -p /usr/local/lib64; \
  else \
    yum -y install epel-release; \
  fi && \
  yum -y install glibc libgcc \
    boost-filesystem boost-program-options boost-system boost-test boost-thread \
    jsoncpp log4cplus yaml-cpp && \
  yum clean all




# Base image for development (contains build tools and headers)
FROM common as development

RUN \
  yum -y install gcc-c++ make cmake3 gmp-devel mpfr-devel libmpc-devel zlib-devel perl rsync && \
  ln -s /usr/bin/cmake3 /usr/bin/cmake && \
  cd /tmp && \
  curl -LO ftp://ftp.mirrorservice.org/sites/sourceware.org/pub/gcc/releases/gcc-8.3.0/gcc-8.3.0.tar.gz && \
  tar xzf gcc-8.3.0.tar.gz && \
  mkdir /tmp/gcc-8.3.0-build && \
  cd /tmp/gcc-8.3.0-build && \
  mkdir /tmp/gcc-products && \
  GCC_CONFIGURE_OPTIONS="--prefix=/tmp/gcc-products --enable-bootstrap --enable-languages=c,c++ --enable-shared --enable-threads=posix --enable-checking=release --enable-multilib --with-system-zlib --enable-__cxa_atexit --disable-libunwind-exceptions --enable-gnu-unique-object --enable-linker-build-id --with-gcc-major-version-only --with-linker-hash-style=gnu --with-default-libstdcxx-abi=gcc4-compatible --enable-plugin --enable-initfini-array --disable-libmpx --enable-gnu-indirect-function" && \
  if [ "${TARGETPLATFORM}" == "linux/arm/v7" ]; then \
    ../gcc-8.3.0/configure ${GCC_CONFIGURE_OPTIONS} --with-tune=cortex-a8 --with-arch=armv7-a --with-float=hard --with-fpu=vfpv3-d16 --with-abi=aapcs-linux; \
  else \
    ../gcc-8.3.0/configure ${GCC_CONFIGURE_OPTIONS}; \
  fi && \
  time make -j4 && \
  time make install && \
  cd / && \
  rm -rf /tmp/gcc-8.3.0* && \
  yum -y remove gcc gcc-c++ && \
  # Copy GCC build products to /usr after RPMs removed - so that RPM removal doesn't delete files from GCC build
  rm /tmp/gcc-products/lib/libgcc_s.so.1 /tmp/gcc-products/lib/libstdc++.so* && \
  rsync -av --links /tmp/gcc-products/ /usr/ && \
  yum clean all

RUN \
  yum -y install rpm-build createrepo git tree \
    boost-devel jsoncpp-devel log4cplus-devel yaml-cpp-devel && \
  yum clean all

# Default user & command
USER root
ENTRYPOINT ["/bin/bash", "-c"]
CMD ["bash"]




# Base image for runtime (only libraries - all already installed above)
FROM common as production

# Default user & command
USER root
ENTRYPOINT ["/bin/bash", "-c"]
CMD ["bash"]
